package com.tyto.qrscanner;

 import android.content.Intent;
 import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
 import android.webkit.WebView;
 import android.webkit.WebViewClient;

public class UserDetailActivity extends AppCompatActivity {

    public static final String EXTRA_STR = "userCV";
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        Intent intent = getIntent();

        String userCV = intent.getStringExtra(EXTRA_STR);

        webView  = (WebView)findViewById(R.id.web_view);


        //Prevent redirect to browser
        webView.setWebViewClient(new WebViewClient());

        //Enable JavaScript
        webView.getSettings().setJavaScriptEnabled(true);

        //Load CV web page
        webView.loadUrl(userCV);
    }


    //Goes back in WebView history, when back button is pressed
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        webView.goBack();

    }
}
