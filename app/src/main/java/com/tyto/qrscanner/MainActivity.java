package com.tyto.qrscanner;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class MainActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private Button scanButton;
    private ZXingScannerView scannerView;
    private Animation fadeInAnim;
    private ImageView logo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        scanButton= ( Button) findViewById(R.id.scan_button);
       // logo = (ImageView) findViewById(R.id.logo_id);


        //Initialize fade in animation
       //fadeInAnim = AnimationUtils.loadAnimation(this, R.anim.fade_in);

        //Set listener for scan button
        scanButton.setOnClickListener(onClickScan);

        //animateViews();

    }

    @Override
    protected void onPause() {
        super.onPause();

        if(scannerView!=null){
        //Stop camera on pause
        scannerView.stopCamera();
        }
    }



    @Override
    protected void onRestart() {
        super.onRestart();

        //Set MainActivity layout after restart activity
        if(scannerView!=null){

            setContentView(R.layout.activity_main);
        }

        //Set OnClickListener for scan button
        scanButton.setOnClickListener(onClickScan);

    }

    //Handle scanning results
    @Override
    public void handleResult(Result result) {

        //Get scanning result
        String scResult = result.getText();

        //Toast.makeText(this,scResult,Toast.LENGTH_LONG ).show();

        Intent intent = new Intent(this, UserDetailActivity.class);
        intent.putExtra(UserDetailActivity.EXTRA_STR, scResult);
        startActivity(intent);

    }


    View.OnClickListener onClickScan= new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            //Initialize the scanner view
            scannerView = new ZXingScannerView(MainActivity.this);
            setContentView(scannerView);

            //Register MainActivity as a handler for scan results
            scannerView.setResultHandler(MainActivity.this);

            //Star camera
            scannerView.startCamera();

        }


    };

/*
    //Get animation method
    private void animateViews(){

        //Animate logo
        logo.setVisibility(View.VISIBLE);
        logo.startAnimation(fadeInAnim);
        //Animate button
        scanButton.setVisibility(View.VISIBLE);
        scanButton.startAnimation(fadeInAnim);


    }

*/
}
